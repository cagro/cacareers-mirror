---
layout: default
title: Back End Engineer
accordion-id: backendEngineer
---

## Back End Engineer Position
**Remote** or **Twin Falls, ID**

**Back End Engineer Position**

KickBack Rewards Systems is seeking a talented back-end software engineer to join our team.
The candidate will work on our data processing pipeline and internal APIs which handle hundreds of gigabytes every day
from millions of devices, phones and sensors. This is a chance to work on challenges of scale in a motivated, fast growing team.
This is an important role that will have a significant impact on the direction of our product and technology.

**Candidate Profile**

You are seriously into development and love problem solving. You take pride in producing quality code and see programming as a form of art. You care about how the software that you and the team produces will be used and like to think of ways it could be done better.

We think that you have an academic background in computer science and have several years of relevant work experience as a developer of primarily backend systems. In your previous work experience we think that you will have been involved with software architecture and system design decisions. You have a strong understanding of principles of good software design and its effect on code quality. 

You have a solid understanding of working agile, specifically in terms of empowerment, values and commitment.

**Responsibilities**

* Design, implement, test and deploy data processing infrastructure
* Research and assess the viability of new processing and data storage technologies
* Participate in design and code reviews

**Required Skills and Experience**

* Expert in developing, designing, and maintaining REST APIs
 * You're picky about your nouns and verbs and labelling your collections.
* Strong and proven object-oriented development skills
* Experience with Python
* At least 2 years of development experience, preferably in a UNIX environment
* Disciplined approach to testing and quality assurance
* Strong written and spoken English
* BS, MS, or PhD in Computer Science or equivalent work experience
* Practical use of Scrum and Agile or similar lean methodologies
* Proven experience with testing methodologies such as TDD

**Beneficial Skills and Experience**

* Large-scale computing experience of multi-threaded applications including high availability, scalability, reliability and distributed systems
* Experience with a few of the following: Hadoop, HBase, Hive, Cassandra, MongoDB, GlusterFS, PostgreSQL, Wowza, Apache, Ruby and Javascript
* Familiarity with NoSQL databases
* Strong command of web standards, CSS-based design, cross-browser compatibility
* Good understanding of web technologies (HTTP, Apache) and familiarity with Unix/Linux
* Knowledgeable foundation in interaction design principles
* Great written communication and documentation abilities
* You are passionate about the craft of writing software
* Hacker mentality
* Must be able to effectively communicate progress and ideas
* B.S. or higher in Computer science or equivalent
