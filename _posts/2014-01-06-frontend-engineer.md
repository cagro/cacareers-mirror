---
layout: default
title: Front End Engineer
accordion-id: frontendEngineer
---

## Front End Engineer Position
**Remote** or **Twin Falls, ID**

**Front End Engineer Position**

KickBack Rewards Systems is seeking a passionate software development professional for the role of Front End Software Engineer.  The ideal candidate will be able to think strategically to provide input on key components of the system, while also rolling-up their sleeves and executing that vision.

**Responsibilities**

* Write front-end code in HTML/CSS and Javascript (AngularJS)
* Implement new features and optimize existing ones from controller-level to UI
* Work closely with, and incorporate feedback from, product management, interaction designers, and back-end engineers
* Rapidly fix bugs and solve problems
* Pro-actively look for ways to make KickBack better

**Required Skills and Experience**

* Demonstrable experience building world-class, consumer web application interfaces
* Expert Javascript/HTML/CSS/Ajax coding skills
* Experience with AngularJS
* Experience with Python or Ruby
* Disciplined approach to testing and quality assurance
* Strong command of web standards, CSS-based design, cross-browser compatibility
* Good understanding of web technologies (HTTP, Apache) and familiarity with Unix/Linux
* Knowledgeable foundation in interaction design principles
* Great written communication and documentation abilities
* You are passionate about the craft of writing software
* Hacker mentality
* Must be able to effectively communicate progress and ideas
* B.S. or higher in Computer science or equivalent
