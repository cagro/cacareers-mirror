---
layout: default
title: Quality Assurance Manager
accordion-id: qualityAssuranceManager
---

**Reports to**: Quality Assurance Manager

**Department**: Quality Assurance

**Classification**: Full-time Permanent

# Summary

Certifies and ensures quality of new and existing KRS products by executing test plans and scripts to verify that new or modified programs, products, and services function according to user requirements and conform to established guidelines.  Communicates and coordinates work efforts and results with impacted clients, customers, and departments under the guidance of the Quality Assurance Manager.

## Essential Functions

* Evaluates and tests new or modified software programs to verify that programs function according to user requirements or implementation needs and will ensure that the programs meet established guidelines, desired outcomes, or API’s for successfully interfacing with intended devices without interfering with the devices primary purpose and functionality.
* Reviews data packets, logs, ngreps, database records, and uses other resources to identify program processing errors or unintended behavior resulting from program modifications or implementations.
* Write, revise, and verify quality standards, test procedures, and processes used to document and report test results and evaluations.
* Develop or use utility programs to test, track, verify, and report defects to provide progress reports.
* Understands the functional requirements of the various systems and processes that would prevent or delay deployment/production delivery and uses good business acumen and situational awareness to recognize when to quickly escalate important defects or deficiencies.
* Reviews new or modified program designs, including documentation, procedures, diagrams, and flow charts, training instructions, etc. to determine if they will inform or perform according to user needs and guidelines.
* Assists with planning and coordinating user acceptance testing, alpha and beta testing.  Ensures that all testing is successfully completed and documented and all problems are resolved before certifying production release.
* Recommend program improvements or corrections to programmers using appropriate communication protocols.
* Assist with development of new interfaces, products, and services as needed.  Use critical-thinking skills to help find alternatives to barriers.
* Monitors program performance after implementation to prevent reoccurrence of program operating problems and ensures efficiency of operation.
* Provides written updates and release notes to users and or in tracking software of program changes, corrections, work progress, test results, test plans, etc. 
* Maintain and configure the internal network, test servers, software, and devices used to perform SQA duties.
* Assist others throughout the organization with the use, implementation of programs, hardware, network topology, and interface options/configurations as needed.
* May assist with education and training with regard to software enhancements and conveying corrections to other employees.
* Helps maintain the appearance and presentation/demonstration capability of the QA lab.
* Assist with fulfillment requirements as needed.

## Knowledge and Skills

* Developing professional expertise. Works on non-complex to moderately complex projects. Exercises judgment within defined procedures and practices.
* General knowledge of software development and testing lifecycles.
* General understanding of test design and testing methodologies.
* Able to perform the software quality certification function with accuracy and timeliness.
* Able to review system and functional requirements and extract information to use in preparing test plans and scripts.
* Able to meet aggressive deadlines and handle multiple projects.
* Able to use independent judgment to plan, prioritize and organize a diversified workload.
* Able to define moderate to complex test cases and scenarios.
* Good planning and organizational skills.
* Well-developed interpersonal and communication skills.
* Basic understanding of software project development concepts and quality control methodologies.
* Familiar with client server environments, UNIX operating system, network topology, and TCP/IP protocol.
* Ability to negotiate effectively and prioritize assignments/tasks.

## Working Conditions

* Typical office setting with environmentally controlled conditions.
* Able to work extended hours as business needs dictate.
* Occasional overnight travel may be required.

## Company Conformance Statement

In performance of their respective responsibilities, all employees of KickBack Rewards Systems are expected to:
* Complete quality work within deadlines.
* Interact professionally with other employees, customers and suppliers.
* Work effectively as a team contributor on all assignments.
* Communicate and coordinate work efforts with other employees, customers and suppliers.
* Perform other duties as assigned.
* Attend work regularly based on the assigned schedule.
* Maintain strict confidentiality in all matters pertaining to client relations, consumer personal information, and individual compensation.

## Minimum Qualifications

* Associate’s or Bachelor’s degree preferred.
* Minimum of 18 months to three years of experience in quality assurance, software testing, or
* A related field; or an equivalent combination of education and experience sufficient to successfully perform the essential functions of the job.
