---
layout: default
title: Data Engineer
accordion-id: dataEngineer
---

## Data Engineer Position
**Remote** or **Twin Falls, ID**

**Data Engineer Position**

As a Data Engineer at KickBack Rewards Systems (KRS), you will be analyzing and extracting insights from one of the world's most interesting data sets.  You will work with decision makers across the company to provide actionable, data-driven answers to their most challenging problems.

**This is an outstanding opportunity to:**

* Work with and learn from leading engineers
* Create a data-driven corporate culture while creating, grooming and developing junior data scientists
* Provide analysis that informs key decisions regarding technical and product direction of the company
* Work with one of the world’s largest and complex consumer behavioral datasets while answering previously unanswerable questions

**We look for a combination of:**

* Experience with Python, Ruby, Scala, R or similar languages like those mentioned.
* Experience with big-data platforms (Hadoop/MapReduce/Hive/Pig)  
* Redis, Cassandra), large-scale log analysis (e.g. Hadoop, Pig, Hive), and big distributed cloud systems (e.g. >20 node cloud deployments)
* Interest in Distributed Systems, Machine Learning, NLP, and search

**Must haves:**

* 1+ years with Hadoop HDFS, Hbase, Hive, Pig and MapReduce.
* 1+ years experience with quantitative analysis and accompanying tools such as R, Python, and Matplotlib
* 1+ years experience with database technologies such as SQL
* Solid programming experience utilizing Java, C, C++ or Python 
* Strong passion for empirical research and both posing and answering hard questions from/with data
* Ability to communicate complex quantitative analyses in a clear, precise, and actionable manner
