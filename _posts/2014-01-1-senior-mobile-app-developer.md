---
layout: default
title: Senior Mobile App Developer
accordion-id: seniorMobileAppDev
---

## Senior Mobile App Developer Position
**Remote** or **Twin Falls, ID**

KickBack Rewards Systems is looking for a senior mobile app developer to lead the design, development and maintenance of our Android and iPhone apps.  KickBack currently has a single application on the app stores called “KickBack Points.”  As a mobile app developer at KickBack, you will be working with us to add valuable features from our road-map in to the applications .  You will lead the entire app lifecycle from concept to delivery and provide post launch support and maintenance. 

These mobile apps provide functionality to our end consumers allowing them to track their progress in games and promotions and receive coupons and promotions.  The mobile apps you develop will integrate with existing web properties and web service apis the KickBack backend team develop in Python and PHP.  In addition to delivering the product the mobile app developer will be heavily involved in driving the mobile strategy globally and will be closely involved with hiring, mentoring and managing our mobile app developers to execute a successful global mobile app strategy for KickBack Rewards Systems.

**Main tasks and responsibilities**

* Act as lead iOS and/or Android developer responsible for writing the application and authoring unit tests. Depending on the skills-set of the candidate this position may be lead on one platform and play a supporting role for the other.
* Work closely with other mobile app developers guaranteeing the vision and direction is cohesive for all platforms maintaining a parity in features.
* Manage the entire app life cycle including concept, design, build, deployment, test, release of the app to the app stores and on-going support of the finished product.
* Work directly with developers and product managers to conceptualize, build, test and realize products.
* Gather requirements around functionality and translate those requirements into elegant functional solutions.
* Build prototypes during scoping stages.
* Work side-by-side with backend developers to create and maintain a robust framework to support the apps.
* Create compelling and invocative device specific user interfaces and experiences.
* Standardize the apps to deliver the same quality app experience across multiple brands with minimal duplication of effort (skinning, branding, etc.)
* Optimizing app performance.  
* Keep up to date on the latest industry trends in the mobile technologies.
* Explain technologies and solutions to technical and non-technical stakeholders.
* Attend industry events/ conference – both attending and presenting

**Requirements**

* Proven commercial software development experience – desktop and mobile.
* Published examples of mobile applications on the iTunes App Store or Google Play market
* Excellent knowledge in information architecture, human computer interaction and usability design principles.
* A track record of delivering successful consumer and or business products.
* Ability to use analytic data and user testing to inform design decision.
* A passion for new consumer technology and the emerging media landscape
* Ability to multi task and good time management skills
* Ability to work on their own and as a part of the team Excellent scoping and estimation skills
* Excellent testing/ QA skills
* Excellent communication skills (verbal and written) to liaise with various departments locally and internationally

**Essential skills:**

* iOS
 * Strong OO design and programming skills in Objective-C
 * Familiar with iOS SDK (UIKit, Cocoa Touch, Core Data, Core Location, etc) 
 * Familiar with xcode
* Android
 * Strong OO design and programming skills in Java (J2EE/ J2ME)
 * Familiar with the Android SDK
 * Knowledge of SQLite, MySQL or similar database management system
 * Familiar with Eclipse
* Common
 * Understanding of other compiled languages
 * Experience on web service integration (SOAP, REST, JSON, XML)
 * Experience of development using web technologies
 * Good understanding of OO programming and design patterns
 * Good understanding of HTML5, JavaScript, jQuery, Ajax and PHP 
 * Experience building web and native apps
 * Experience using social media APIs
 * Experience using version control (e.g. git, svn, etc.)
 * Excellent debugging and optimization skills
 * Exceptional grounding in mobile user-experience.
 * A “code as a craft” theology.  You're willing to make beautiful works of art for KickBack.
